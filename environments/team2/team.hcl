# Set common variables for the team. This is automatically pulled in in the
# root terragrunt.hcl configuration to feed forward to the child modules.
locals {
  team = "team2"
}
