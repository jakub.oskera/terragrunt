# Set common variables for the environment. This is automatically pulled in in the
# root terragrunt.hcl configuration to feed forward to the child modules.
locals {
  subscription_id      = "aba2b248-b8aa-4294-814d-b5d2600cf9cb"
  resource_group_name  = "rg-dev-team2"
  storage_account_name = "exampledevteam2"
  environment          = "dev"
}
