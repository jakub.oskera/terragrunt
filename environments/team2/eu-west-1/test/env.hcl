# Set common variables for the environment. This is automatically pulled in in the
# root terragrunt.hcl configuration to feed forward to the child modules.
locals {
  subscription_id      = "XXX"  # FIXME: use environment variable
  resource_group_name  = "rg-test-team2"
  storage_account_name = "exampletestteam2"
  environment          = "test"
}
