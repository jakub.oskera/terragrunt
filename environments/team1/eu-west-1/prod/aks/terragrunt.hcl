# Include the root `terragrunt.hcl` configuration. The root configuration
# contains settings that are common across all components and environments,
# such as how to configure remote state.
include "root" {
  path = find_in_parent_folders()
}

# Include the envcommon configuration for the component. The envcommon
# configuration contains settings that are common for the component
# across all environments.
include "envcommon" {
  path = "${dirname(find_in_parent_folders())}/environments/team1/_envcommon/aks.hcl"
  # We want to reference the variables from the included config in this configuration, so we expose it.
  //   expose = true
}

# Configure the version of the module to use in this environment. This allows
# you to promote new versions one environment at a time
terraform {
  //   source = "${include.envcommon.locals.base_source_url}?ref=v0.8.0"
  source = "${get_parent_terragrunt_dir("root")}/modules//aks"
}

inputs = {
  cluster_name  = "prod-euw-team1"
  dns_prefix    = "prodeuwteam"
  client_secret = "XXX"    # FIXME: use environment variable
}

