# Set common variables for the region. This is automatically pulled in in the
# root terragrunt.hcl configuration to feed forward to the child modules.
locals {
  location = "westeurope"
}
