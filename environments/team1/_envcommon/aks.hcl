# -----------------------------------------------------------------------------
# COMMON TERRAGRUNT CONFIGURATION
# This is the common component configuration for aks. The common variables for
# each environment to deploy aks are defined here. This configuration will be
# merged into the environment configuration via an include block.
# -----------------------------------------------------------------------------

locals {
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))
}

# -----------------------------------------------------------------------------
# MODULE PARAMETERS
# These are the variables we have to pass in to use the module. This defines
# the parameters that are common across all environments.
# -----------------------------------------------------------------------------
inputs = {
  location            = local.region_vars.locals.location
  resource_group_name = local.environment_vars.locals.resource_group_name
  kubernetes_version  = "1.28.9"
  node_count          = 1
  vm_size             = "standard_d2_v2"
  max_surge           = "10%"
  # FIXME: use variable from too terragrunt.hcl, so we do not have to speciy the tags for each module separatly
  tags = {
    Entity = "Example"
  }
}
