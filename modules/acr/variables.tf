variable "acr_name" {
  description = "The name of the ACR"
  type        = string
}

variable "resource_group_name" {
  description = "The name of the resource group"
  type        = string
}

variable "location" {
  description = "The Azure location"
  type        = string
}

variable "tags" {
  description = "Tags to apply to the resources"
  type        = map(string)
}
