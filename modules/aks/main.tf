resource "azurerm_kubernetes_cluster" "this" {
  name                = var.cluster_name
  location            = var.location
  resource_group_name = var.resource_group_name
  dns_prefix          = var.dns_prefix
  kubernetes_version  = var.kubernetes_version

  default_node_pool {
    name       = "default"
    node_count = var.node_count
    vm_size    = var.vm_size

    upgrade_settings {
      max_surge = var.max_surge
    }
  }

  identity {
    type = "SystemAssigned"
  }

  tags = var.tags
}

# FIXME
# resource "azurerm_role_assignment" "aks_dns_contributor" {
#   principal_id         = azurerm_kubernetes_cluster.this.kubelet_identity[0].object_id
#   role_definition_name = "Contributor"
#   scope                = data.azurerm_dns_zone.this.id
# }

resource "helm_release" "ingress-nginx" {
  name             = "ingress-nginx"
  repository       = "https://kubernetes.github.io/ingress-nginx"
  chart            = "ingress-nginx"
  namespace        = "ingress-nginx"
  version          = "4.10.1"
  create_namespace = true
  set {
    name  = "controller.replicaCount"
    value = "3"
  }

  # FIXME:
  # set {
  #   name  = "controller.extraArgs.default-ssl-certificate"
  #   value = "$(POD_NAMESPACE)/${format("wildcard.%s", data.azurerm_dns_zone.dev_team1_example_com.name)}"
  # }

  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-health-probe-request-path"
    value = "/healthz"
  }

  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  set {
    name  = "controller.config.proxy-body-size"
    value = "0"
  }

  set {
    name  = "controller.config.proxy-connect-timeout"
    value = "60"
  }

  set {
    name  = "controller.config.proxy-read-timeout"
    value = "60"
  }

  set {
    name  = "defaultBackend.enabled"
    value = "true"
  }

  depends_on = [
    azurerm_kubernetes_cluster.this
  ]
}

resource "helm_release" "cert_manager" {
  name             = "cert-manager"
  repository       = "https://charts.jetstack.io"
  chart            = "cert-manager"
  namespace        = "cert-manager"
  version          = "v1.14.5"
  create_namespace = true
  wait             = true
  set {
    name  = "installCRDs"
    value = true
  }

  depends_on = [
    azurerm_kubernetes_cluster.this
  ]
}

resource "kubernetes_manifest" "secret_cert_manager_azuredns_config" {
  manifest = {
    apiVersion = "v1"
    kind       = "Secret"
    metadata = {
      name      = "azuredns-config"
      namespace = "${helm_release.cert_manager.namespace}"
    }
    data = {
      client-secret = base64encode(var.client_secret)
    }
    type = "Opaque"
  }
}
