locals {
  # Automatically load team-level variables
  team_vars = read_terragrunt_config(find_in_parent_folders("team.hcl"))

  # Automatically load region-level variables
  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl"))

  # Automatically load environment-level variables
  environment_vars = read_terragrunt_config(find_in_parent_folders("env.hcl"))

  # Extract the variables we need for easy access
  team                 = local.team_vars.locals.team
  location             = local.region_vars.locals.location
  subscription_id      = local.environment_vars.locals.subscription_id
  resource_group_name  = local.environment_vars.locals.resource_group_name
  storage_account_name = local.environment_vars.locals.storage_account_name
  environment          = local.environment_vars.locals.environment

  # FIXME: should be environment variables
  //   tenant_id            = get_env("ARM_TENANT_ID")
  tenant_id = "XXX"  # FIXME: use environment variable
  //   client_id            = get_env("ARM_CLIENT_ID")
  client_id = "XXX"  # FIXME: use environment variable
  //   client_secret        = get_env("ARM_CLIENT_SECRET")
  client_secret = "XXX"  # FIXME: use environment variable
}


# Generate providers versions
generate "versions" {
  path      = "versions.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "3.102.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.13.2"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.30.0"
    }
  }
}
EOF
}

# Generate providers
generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "azurerm" {
  subscription_id = "${local.subscription_id}"
  tenant_id       = "${local.tenant_id}"
  client_id       = "${local.client_id}"
  client_secret   = "${local.client_secret}"
  features {}
}

provider "helm" {
  kubernetes {
    host                   = azurerm_kubernetes_cluster.this.kube_config.0.host
    client_certificate     = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.client_key)
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.cluster_ca_certificate)
  }
}

provider "kubernetes" {
  host                   = azurerm_kubernetes_cluster.this.kube_config.0.host
  client_certificate     = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.client_key)
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.this.kube_config.0.cluster_ca_certificate)
}

EOF
}


# Configure Terragrunt to automatically store tfstate files in an Blob Storage
# container
remote_state {
  backend = "azurerm"
  generate = {
    path      = "backend.tf"
    if_exists = "overwrite"
  }
  config = {
    subscription_id      = local.subscription_id
    resource_group_name  = local.resource_group_name
    storage_account_name = local.storage_account_name
    container_name       = "terraform"
    key                  = "${path_relative_to_include()}/terraform.tfstate"
  }
}


# -----------------------------------------------------------------------------
# GLOBAL PARAMETERS
# These variables apply to all configurations in this subfolder. These are
# automatically merged into the child `terragrunt.hcl` config via the include block.
# -----------------------------------------------------------------------------

# Configure root level variables that all resources can inherit. This is
# especially helpful with multi-account configs where terraform_remote_state
# data sources are placed directly into the modules.
inputs = merge(
  local.team_vars.locals,
  local.region_vars.locals,
  local.environment_vars.locals,
  # FIXME: make local tags available in all terragrunt.hccl for acr, aks etc. to use it as input for module
  //   tags = {
  //     Entity = "Example"
  //   },
)
